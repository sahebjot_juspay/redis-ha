#!/bin/bash
kubectl get -o json rc | jq '.items[].metadata.name' | grep redis | while read line; do pod=$(echo $line | tr -d '"'); kubectl delete rc $pod --grace-period=0 --force ;done
kubectl get -o json svc | jq '.items[].metadata.name' | grep redis | while read line; do pod=$(echo $line | tr -d '"'); kubectl delete svc $pod --grace-period=0 --force ;done
kubectl get -o json pods | jq '.items[].metadata.name' | grep redis | while read line; do pod=$(echo $line | tr -d '"'); kubectl delete pod $pod --grace-period=0 --force ;done
# kubectl delete rc --all --grace-period=0 --force
# kubectl delete service redis-sentinel --grace-period=0 --force
# kubectl delete pods --all --grace-period=0 --force

