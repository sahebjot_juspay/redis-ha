#!/bin/bash
kubectl apply -f redis-master.yaml
sleep 5;
kubectl apply -f redis-sentinel-service.yaml
kubectl apply -f redis-controller.yaml
kubectl apply -f redis-sentinel-controller.yaml
kubectl scale rc redis --replicas=3
kubectl scale rc redis-sentinel --replicas=3

# pods=$(kubectl get -o json pods | jq '.items[].metadata.name')
# echo $pods
# sentinels=$(echo $pods | grep redis-sentinel)
# redis=$(echo $pods | grep "redis" | grep -v "sentinel")

while true; do
  sentinel_output=$(kubectl get -o json pods | jq '.items[].metadata.name' | grep redis-sentinel | while read line; do pod=$(echo $line | tr -d '"');  kubectl logs $pod | grep -c "monitor master"; done)
  sum=0
  for word in $sentinel_output
  do
    ((sum+=$word))
  done
  if [ $sum == 3 ]; then
    echo "Sentinels connected to master"
    while true; do
      redis_pod_output=$(kubectl get -o json pods | jq '.items[].metadata.name' | grep redis | grep -v sentinel | grep -v master | while read line; do pod=$(echo $line | tr -d '"');  kubectl logs $pod "redis" | grep -c "Master replied to PING, replication can continue..."; done)
      sum=0
      for pod_output in $redis_pod_output
      do
        ((sum+=pod_output))
      done
      if [ $sum == 2 ]; then
        echo "Redis cluster ready"
        sleep 1
        kubectl delete pods redis-master --grace-period=0 --force
        exit 0;
      else
        echo "slaves not connected to master.."
        sleep 1
      fi
    done
  else
    echo "sentinels not ready.."
    sleep 1
  fi
done


