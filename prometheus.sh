#!/bin/bash
kubectl get -o json pods | jq '.items[].metadata.name' | grep redis | grep -v sentinel | while read line; do pod=$(echo $line | tr -d '"'); kubectl exec $pod redis-cli info | grep role | grep master ; done


